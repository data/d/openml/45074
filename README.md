# OpenML dataset: Shipping

https://www.openml.org/d/45074

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

An international e-commerce company based wants to discover key insights from their customer database. They want to use some of the most advanced machine learning techniques to study their customers. The company sells electronic products

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45074) of an [OpenML dataset](https://www.openml.org/d/45074). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45074/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45074/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45074/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

